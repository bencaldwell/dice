/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.caldwellsoftware.dice;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.apache.log4j.Logger;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Supplier;


/**
 *
 * @author bencaldwell
 */
public class Dice {
    
    private final List<Gene> inputs;
    private final List<Gene> genes;
    private final List<Gene> outputs;
    private List<Genotype> population;
    private int popSize;
    private int maxAllowedDepth;
    private final int tournamentSize;
    private int maxIterations;
    private int generationCounter=0;
    private double parsimonyFitness;
    private final double mutationProbability;
    private final double argVariance;
    private final Random rand;
    private final long randomSeed;
    // hooks for users of the framework to evaluate populations and individuals
    private final Optional<Consumer<Void>> startPopulationExpression;
    private final Optional<Consumer<Dice>> endPopulationExpression;
    private final Optional<Consumer<Genotype>> startIndividualExpression;
    private final Optional<Consumer<Genotype>> endIndividualExpression;
    private final Optional<Consumer<Genotype>> startIndividualEvaluation;
    private final Optional<Consumer<Genotype>> endIndividualEvaluation;
    
    static Logger log = Logger.getLogger(Dice.class.getName());
    
    public static class Builder {
            
        private List<Gene> genes = new ArrayList<>();
        private List<Gene> inputs = new ArrayList<>();
        private List<Gene> outputs = new ArrayList<>();
        private List<Genotype> population = new ArrayList<>();
        private Optional<Consumer<Void>> startPopulationExpression;
        private Optional<Consumer<Dice>> endPopulationExpression;
        private Optional<Consumer<Genotype>> startIndividualExpression;
        private Optional<Consumer<Genotype>> endIndividualExpression;
        private Optional<Consumer<Genotype>> startIndividualEvaluation;
        private Optional<Consumer<Genotype>> endIndividualEvaluation;
        
        private int popSize = 2;
        private int maxDepth = 5;
        private int tournamentSize = 7; // [Sean Luke: traditional tournament size for GP is 7]
        private int maxIterations = 10;
        private double mutationProbability = 0.1;
        private double argVariance = 0.1;
        private double parsimonyFitness = 1.0;
        private long randomSeed;
        
        public Builder() {
            startPopulationExpression = Optional.empty();
            endPopulationExpression = Optional.empty();
            startIndividualExpression = Optional.empty();
            endIndividualExpression = Optional.empty();
            endIndividualEvaluation = Optional.empty();
            startIndividualEvaluation = Optional.empty();
            randomSeed = new Random().nextLong();
        }

        public void setRandomSeed(long randomSeed) {
            this.randomSeed = randomSeed;
        }

        public void setStartIndividualEvaluation(Consumer<Genotype> startIndividualEvaluation) {
            this.startIndividualEvaluation = Optional.of(startIndividualEvaluation);
        }

        public void setEndIndividualEvaluation(Consumer<Genotype> endIndividualEvaluation) {
            this.endIndividualEvaluation = Optional.of(endIndividualEvaluation);
        }
       
        public Builder setStartPopulationExpression(Consumer<Void> expression) {
            this.startPopulationExpression = Optional.of(expression);
            return this;
        }
        
        public Builder setEndPopulationExpression(Consumer<Dice> expression) {
            this.endPopulationExpression = Optional.of(expression);
            return this;
        }
        
        public Builder setStartIndividualExpression(Consumer<Genotype> expression) {
            this.startIndividualExpression = Optional.of(expression);
            return this;
        }
        
        public Builder setEndIndividualExpression(Consumer<Genotype> expression) {
                this.endIndividualExpression = Optional.of(expression);
            return this;
        }
        
        public Builder addInputs(List<? extends Gene> inputs) {
            this.inputs.addAll(inputs);
            return this;
        }
        
        public Builder addGenes(List<? extends Gene> genes) {
            this.genes.addAll(genes);
            return this;
        }
        
        public Builder addOutputs(List<? extends Gene> outputs) {
            this.outputs.addAll(outputs);
            return this;
        }

        public Builder setPopSize(int popSize) {
            this.popSize = popSize;
            return this;
        }

        public void setTournamentSize(int tournamentSize) {
            this.tournamentSize = tournamentSize;
        }
        
        public Builder setMaxDepth(int maxDepth) {
            this.maxDepth = maxDepth;
            return this;
        }

        public void setMaxIterations(int maxIterations) {
            this.maxIterations = maxIterations;
        }

        public void setMutationProbability(double mutationProbability) {
            this.mutationProbability = mutationProbability;
        }

        public void setArgVariance(double argVariance) {
            this.argVariance = argVariance;
        }

        public void setParsimonyFitness(double parsimonyFitness) {
            this.parsimonyFitness = parsimonyFitness;
        }
        
        public Dice build() {
            return new Dice(this);
        }
    }
    
    private Dice(Builder builder) {
        
        this.randomSeed = builder.randomSeed;
        this.rand = new Random(this.randomSeed);
        this.inputs = builder.inputs;
        this.genes = builder.genes;
        this.outputs = builder.outputs;
        this.population = builder.population;
        this.popSize = builder.popSize;
        this.maxAllowedDepth = builder.maxDepth;
        this.tournamentSize = builder.tournamentSize;
        this.maxIterations = builder.maxIterations;
        this.mutationProbability = builder.mutationProbability;
        this.argVariance = builder.argVariance;
        this.parsimonyFitness = builder.parsimonyFitness;
        this.startPopulationExpression = builder.startPopulationExpression;
        this.endPopulationExpression = builder.endPopulationExpression;
        this.startIndividualExpression = builder.startIndividualExpression;
        this.endIndividualExpression = builder.endIndividualExpression;
        this.startIndividualEvaluation = builder.startIndividualEvaluation;
        this.endIndividualEvaluation = builder.endIndividualEvaluation;
    }
    
    public void create() {
        // use a lambda function to supply random copies of genes where required
        Supplier<Gene> geneSupplier = () -> {
            int selector = rand.nextInt(genes.size());
                Gene gene = genes.get(selector);
                return new Gene(gene); // return a deep copy of the gene for use
        };
        
        // create the members of the population - provide the gene supplier
        while (population.size() < this.popSize) {
            int individualNum = population.size();
            population.add(new Genotype(inputs, 
                                        outputs, 
                                        geneSupplier, 
                                        this.maxAllowedDepth, 
                                        rand, 
                                        generationCounter, 
                                        individualNum,
                                        mutationProbability,
                                        argVariance));
        }
    }

    
    public void start() throws Exception {
        // create a population if it doesn't exist
        if (population==null || population.size() < this.popSize) {
            this.create();
            //evaluate a population
            this.evaluate();
        }
        
        //killoff
        for (int i=0; i<this.maxIterations; i++) {
            generationCounter++;
            this.breed();
            //TODO: evaluate again until goal reached
            this.evaluate();
        }
    }
    
    
    private void evaluate() {

        // call the startPopulation callback if provided
        try {
            this.startPopulationExpression.get().accept(null);
        } catch (NoSuchElementException e) {
            log.debug("No startPopulation callback provided."); // empty hook don't do anything
        }
        
        // express the individual
        this.population.stream().forEach((individual) -> {
            
            // call the startIndividual callback if provided
            try {
                this.startIndividualExpression.get().accept(individual);
            } catch (NoSuchElementException e) {
                log.debug("No startIndividual callback provided."); // empty hook don't do anything
            }
            
            // express the individual
            individual.express();
            
            // call the endIndividual callback if provided
            try {
                this.endIndividualExpression.get().accept(individual);
            } catch (NoSuchElementException e) {
                log.debug("No endIndividual callback provided."); // empty hook don't do anything
            }
            
            // get the fitness results for the individual and store
            this.startIndividualEvaluation.get().accept(individual);
            
            // record data for this expressed and evaluated individual
            this.endIndividualEvaluation.get().accept(individual);
        }); 
        
        // call the endPopulation callback if provided
        try {
            this.endPopulationExpression.get().accept(this);
        } catch (NoSuchElementException e) {
            log.debug("No endPopulation callback provided."); // empty hook don't do anything
        }
        
    }
       
    /**
     * Breed a new generation of candidate solutions
     */
    private void breed() {
        List<Genotype> newPopulation = new ArrayList<>();
        while (newPopulation.size() < this.popSize) {
            // select two copies of "parents"
            int individualNum1 = newPopulation.size();
            int individualNum2 = individualNum1 + 1;
            Genotype new1 = selection(individualNum1);
            Genotype new2 = selection(individualNum2);
            // perform crossover of outputs
            new1.crossover(new2);
            // mutate each genotype
            new1.mutate();
            new2.mutate();
            // add new "children" to the next generation
            newPopulation.add(new1);
            newPopulation.add(new2);
        }
        this.population = newPopulation; 
    }
    
    /**
     * Tournament selection is used to select the fittest individuals from the population
     */
    private Genotype selection(int individualNum) {
        // tournament selection   [Luke, Sean:  "Essentials of Metaheuristics"]
        Genotype best = this.population.get(rand.nextInt(this.population.size()));
        for (int i=0; i<this.tournamentSize; i++) {
            Genotype next = this.population.get(rand.nextInt(this.population.size()));
            // select the fittest
            if (next.getFitness() > best.getFitness()) {
                best = next;
            } 
            // ...or if the same fitness select the smallest (parsimony pressure)
            // only do this when fitness is above a threshold level - parsimonyFitness
            else if (next.getFitness() == best.getFitness() && next.getGeneCount() < best.getGeneCount() && best.getFitness() >= parsimonyFitness) {
                best = next;
            }
        }
        return new Genotype(best, generationCounter, individualNum); // return a copy so the original population is left intact
    }
    
    /**
     * Get the average fitness of all individuals.
     * @return 
     */
    public double getAverageFitness() {
        double totalFitness = 0.0;
        for (Genotype individual : this.population) {
            totalFitness += individual.getFitness();
        }
        double averageFitness = totalFitness / this.population.size();
        return averageFitness;
    }
    
    public double getAverageGeneCount() {
        double totalGeneCount = 0.0;
        for (Genotype individual : this.population) {
            totalGeneCount += individual.getGeneCount();
        }
        double averageGeneCount = totalGeneCount / this.population.size();
        return averageGeneCount;
    }
    
    public int getMaxDepth() {
        return maxAllowedDepth;
    }

    public void setMaxAllowedDepth(int maxDepth) {
        this.maxAllowedDepth = maxDepth;
        for (Genotype individual : this.population) {
            individual.setMaxAllowedDepth(this.maxAllowedDepth);
        }
    }

    public void setMaxIterations(int maxIterations) {
        this.maxIterations = maxIterations;
    }

    public void setPopSize(int popSize) {
        this.popSize = popSize;
    }

    public void setParsimonyFitness(double parsimonyFitness) {
        this.parsimonyFitness = parsimonyFitness;
    }

    public int getPopSize() {
        return popSize;
    }

    public long getRandomSeed() {
        return randomSeed;
    }

}    
