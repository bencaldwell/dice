/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.caldwellsoftware.dice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import org.apache.log4j.Logger;

/**
 *
 * @author bencaldwell
 */
public class Gene {

    private final String name;    
    private final int instanceNum;
    private Gene output;
    private List<Gene> inputs = new ArrayList<>();
    private final int arity;
    public Map<String, String> constants = new HashMap<>();
    public Map<String, Double> parameters = new HashMap<>(); // mutatable parameters
    static final Logger logger = Logger.getLogger(Gene.class.getName());
    private static int instanceCounter = 0;
    private final Consumer<? super Gene> expressor;
    private Boolean isCommutative = false;
    
    public Gene(String name, 
                int arity,
                boolean isCommutative,
                Map<String, String> constants,
                Map<String, Double> parameters,
                Consumer<? super Gene> expressor) {
        
        this.name = name;
        this.instanceNum = instanceCounter++;
        this.arity = arity;
        this.isCommutative = isCommutative;
        this.expressor = expressor;
        
        // create deep copies of mutable objects/lists
        if (constants != null) {
            for (Map.Entry<String, String> constant : constants.entrySet()) {
                this.constants.put(new String(constant.getKey()), new String(constant.getValue()));
            } 
        }
        if (parameters != null) {
            for (Map.Entry<String, Double> parameter : parameters.entrySet()) {
                this.parameters.put(new String(parameter.getKey()), new Double(parameter.getValue()));
            }
        }
    }
        
    /**
     * Copy constructor.
     * @param src 
     */
    public Gene(Gene src) {
        // new Gene with the same name
        this(src.name, src.arity, src.isCommutative, src.constants, src.parameters, src.expressor);
    }
    
    /**
     * Copy the subtree starting with this gene
     * 
     * @return newGene: the head of the copied subtree
     */
    public Gene copySubTree() {
        // copy this gene
        Gene newGene = new Gene(this);

        // copy all inputs down the tree and relink as per original
        for (Gene input : this.inputs) {
            Gene inputGene = input.copySubTree();
            newGene.inputs.add(inputGene);
            inputGene.setOutput(newGene);
        }

        return newGene;
    }
            
    public Gene getOutput() {
        return this.output;
    }
    
    public void setOutput(Gene output) {
        this.output = output;
    }
    
    public List<Gene> getInputs() {
        return this.inputs;
    }
    
    public void initInputs(Function<Integer, ? extends Gene> inputSupplier) {
        for (int i=0; i<arity; i++) {
            Gene input = inputSupplier.apply(this.getDepth()); // get a random input
            this.inputs.add(input); // store the input
            input.setOutput(this); // set the inputs output to point back to this gene
            input.initInputs(inputSupplier); // init the input's inputs (recursive operation)
        }
    }
    
    /**
     * Add an input gene between this gene and one of its inputs.
     * 
     * One of the subtrees is the old input subtree, the rest are newly generated.
     * @param inputSupplier 
     */
    public void insertInputBefore(Gene oldInput, Function<Integer, ? extends Gene> inputSupplier, Random rand, int maxAllowedDepth) {
        // don't insert if it would violate the max depth constraint
        if (this.getSubTreeDepth() >= maxAllowedDepth) return;
        int inputIndex = this.inputs.indexOf(oldInput);
        Gene newInput = inputSupplier.apply(this.getDepth()); // get a random input
        newInput.setOutput(this); // point the output of the new input back to this gene
        this.inputs.remove(oldInput); // remove the old input
        this.inputs.add(inputIndex, newInput); // add the new input at the same index ("parameter") as the old one
        if (newInput.isInput()) {
            return; // just drop the subtree if the new input is an "Input" gene
        } else {
            newInput.initInputs(inputSupplier); // randomly initialize the new gene's inputs
            int inputForOldTree = rand.nextInt(newInput.arity); // randomly choose an input for the old subtree
            newInput.inputs.remove(inputForOldTree); // remove the random subtree from the chosen input
            newInput.inputs.add(inputForOldTree, oldInput); // add the old subtree to the chosen input
            oldInput.setOutput(newInput); // the output of the old input is the new input
        }
    }
    
    /**
     * Remove an input gene to this gene and drop all but one of its subtrees
     * 
     * One of the subtrees becomes the new input to this gene
     * @param inputSupplier 
     */
    public void removeInput(Gene oldInput, Function<Integer, ? extends Gene> inputSupplier, Random rand) {
        // if the input to remove is an Input gene then do nothing - can't pull up a non-existent subtree
        if (oldInput.isInput()) return;
        int inputToKeep = rand.nextInt(oldInput.arity); // randomly select which subtree to keep
        Gene newInput = oldInput.inputs.get(inputToKeep); // get the head gene of the kept subtree
        newInput.setOutput(this); // the output of the new input is now this gene
        int inputIndex = this.inputs.indexOf(oldInput); // get the input number of the kept subtree
        this.inputs.remove(oldInput); // remove the old input
        this.inputs.add( inputIndex, newInput); // reattach the subtree
    }
    
    /**
     * Swap a gene with another gene of same arity.
     * 
     * @param oldInput
     * @param inputSupplier
     * @param rand 
     */
    public void swapGene(Gene oldInput, Function<Integer, ? extends Gene> inputSupplier, Random rand) {
        Gene newInput;
        do {
            newInput = inputSupplier.apply(this.getDepth());
        } while (newInput.arity != oldInput.arity);
        
        // Copy all the inputs of the old gene to the new gene
        for (Gene input : oldInput.inputs) {
            newInput.inputs.add(input);
        }
        
        // Remove the old gene and replace with the new gene
        int inputIndex = this.inputs.indexOf(oldInput); // get the input number of the kept subtree
        this.inputs.remove(oldInput); // remove the old input
        this.inputs.add( inputIndex, newInput); // reattach the subtree
    }
    
    /**
     * Drop an input and completely regenerate its subtree.
     * 
     * @param oldInput
     * @param inputSupplier
     * @param rand 
     */
    public void regenerateInput(Gene oldInput, Function<Integer, ? extends Gene> inputSupplier, Random rand) {
        logger.debug(String.format("Regenerating input gene tree: %1$s", oldInput.getName()));
        int inputIndex = this.inputs.indexOf(oldInput);
        this.inputs.remove(oldInput);
        Gene newInput = inputSupplier.apply(this.getDepth()); // get a random input
        newInput.setOutput(this);
        newInput.initInputs(inputSupplier);
        this.inputs.add(inputIndex, newInput);
    }
    
    /**
     * Mutate the subtree of this gene, only if the right random number is generated.
     * 
     * @param probability 
     */
    public void mutateSubtreeStructure(double probability, Random rand, Function<Integer, ? extends Gene> inputSupplier, int maxAllowedDepth) {
        
        // if we are not regenerating this subtree walk down the inputs and stochastically regenerate their subtrees
        // use a plain for loop (not for each) to prevent concurrent modification exceptions
        for (int i=0; i<this.inputs.size(); i++) {
            Gene input = this.inputs.get(i);
            // if a random number is less than the mutation probability, mutate the subtree
            if (rand.nextDouble() <= probability) {
                double mutType = rand.nextDouble();

                // add a gene at the input
                if (mutType < (1d/4d)){
                    this.insertInputBefore(input, inputSupplier, rand, maxAllowedDepth);
                }

                // remove a gene at the input
                else if (mutType < (2d/4d)){
                    this.removeInput(input, inputSupplier, rand);
                }

                // drop the input and regenerate
                else if (mutType < (3d/4d)){
                    this.regenerateInput(input, inputSupplier, rand);
                }
                
                //TODO: replace a gene with another of the same arity (if one exists)
                else {
                        this.swapGene(input, inputSupplier, rand);
                }
            } 
            else {
                // recurse through the tree
                input.mutateSubtreeStructure(probability, rand, inputSupplier, maxAllowedDepth);
            }
        }
    }
    
    /**
     * Mutate all mutatable params using Gaussian convolution.
     * @param variance 
     */
    public void mutateSubtreeParams(double variance, Random rand) {
        // perform recursively for all genes in the subtree
        for (Gene input : this.inputs) {
            input.mutateSubtreeParams(variance, rand);
        }
        // perform Gaussian convolution on this gene's mutatable parameters
        if (parameters != null) {
            for (Map.Entry<String, Double> param : parameters.entrySet()) {
                Double value = param.getValue();
                value = rand.nextGaussian() * variance * value + value;
                logger.debug(param.getKey() + " value changed from " 
                        + param.getValue() + " to " + value);
                param.setValue(value);
            }
        }
    }
    
    public void express() {
        this.inputs.stream().forEach((input) -> {
            input.express();
        });
        this.expressor.accept(this);
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getUniqueName() {
        return this.name + String.format("%03d", this.instanceNum);
    }
    
    public int getInstanceNum() {
        return this.instanceNum;
    }
    
    /**
     * getDepth returns the depth down the tree from an output.
     * If this node is an output the depth is 1.
     * @return 
     */
    public int getDepth() {
        if (this.isOutput()) {
            return 1;
        } else {
            return this.getOutput().getDepth() + 1;
        }
    }
    
    /**
     * getDepth returns the depth up the tree from an input.
     * If this node is an input the depth is 1.
     * @return 
     */
    public int getDepthInverse() {

        // return 1 if this is an input (arity==0)
        if (this.isInput()) {
            return 1;
        } 
        
        // genes that are not an input start with inverse depth of 2 (the input is 1)
        int depth = 2; 
        for (Gene input : inputs) {
            int newDepth = input.getDepthInverse() + 1;
            depth = (newDepth > depth) ? newDepth : depth;
        }
        
        return depth;
    }
    
    /**
     * Get the total depth of the subtree including this node
     * @return 
     */
    public int getSubTreeDepth() {
        int subTreeDepth = this.getDepth();
        subTreeDepth = subTreeDepth - 1 + this.getDepthInverse();
        return subTreeDepth;
    }

    
    public int getGeneCount() {
        
        // return 1 if this is an input (arity==0)
        if (this.isInput()) {
            return 1;
        } 
        
        // count this gene (including if it is an output)
        int count = 1; 
        for (Gene input : inputs) {
            count += input.getGeneCount();
        }
        
        return count;
    }

    @Override
    public String toString() {
        return this.name;
    }
    
    public String printTree() {
        return printTree("", true);
    }
    
    private String printTree(String prefix, boolean isTail) {
        String tree = System.lineSeparator() + prefix + (isTail ? "└── " : "├── ") + name;
        // add mutable parameters
        if (parameters != null) {
            StringBuilder builder = new StringBuilder();
            for (Map.Entry<String, Double> parameter : parameters.entrySet()) {
                if (builder.length() != 0) {
                    builder.append(",");
                } else {
                    builder.append("(");
                }
                builder.append(parameter.toString());
            }
            builder.append(")");
            tree = tree + builder.toString();
        }
        for (int i = 0; i < this.inputs.size() - 1; i++) {
            tree = tree + this.inputs.get(i).printTree(prefix + (isTail ? "    " : "│   "), false);
        }
        if (this.inputs.size() > 0) {
            tree = tree + this.inputs.get(this.inputs.size() - 1).printTree(prefix + (isTail ?"    " : "│   "), true);
        }
        
        return tree;
    }
    
    public void createGraph(StringBuilder sb) {
        // make the node a box if it is an input or output
        if (this.isInput() || this.isOutput()) {
            sb.append(this.getUniqueName());
            sb.append(" [shape=box];");
            sb.append(System.lineSeparator());
        }
        // make the relationships between nodes
        for (Gene input : this.inputs) {
            // declare the next node
            sb.append(input.getUniqueName());
            // add mutable parameters of the next node to its label
            if (!input.parameters.isEmpty()) {
                sb.append(" [label=\"").append(input.getUniqueName());
                for (Map.Entry<String,Double> entry : input.parameters.entrySet()) {
                    sb.append("\\n");
                    sb.append(entry.getKey()).append(":").append(String.format("%.2f",entry.getValue()));
                }
                sb.append("\"]");
            }
            sb.append(System.lineSeparator());
            // declare the link between this node and the next node
            sb.append(this.getUniqueName());
            sb.append("->");
            sb.append(input.getUniqueName());
            sb.append(" [label=" + this.inputs.indexOf(input) +"]");
            sb.append(";");
            sb.append(System.lineSeparator());
            // recurse into the next node
            input.createGraph(sb);
        }
    }
    
    public boolean isInput() {
        // an input has 0 arity
        return this.arity==0;
    }
    
    public boolean isOutput() {
        // an output has no output after it
        return this.output == null;
    }
    
    /**
     * Get the text expression of the program graph.
     * 
     * Format example "OUT0=AND(IN1,OR(IN2,IN3));OUT1=OR(IN4,IN5);"
     * @param sb 
     */
    public void getExpression(StringBuilder sb) {
        String inputOutput = "(";
        if (this.isOutput()) {
            inputOutput = "=";
        } else if (this.isInput()) {
            inputOutput = "";
        }
        
        sb.append(this.getName());
        sb.append(inputOutput);
        
        // get a list of input expressions to this gene
        List<String> inputExpressions = new ArrayList<>();
        for (Gene input : this.inputs) {
            StringBuilder sbSort = new StringBuilder();
            input.getExpression(sbSort);
            inputExpressions.add(sbSort.toString());
        }
        
        // sort the inputs if commutative to give consistent strings for equivalent expressions
        if (this.isCommutative) {
            inputExpressions.sort(String::compareToIgnoreCase);
        }
        
        // append the input expressions - sorted if this is commutative
        String delim = "";
        for (String inputString : inputExpressions) {
            sb.append(delim);
            sb.append(inputString);
            delim=",";
        }
        
        inputOutput = ")";
        if (this.isOutput()) {
            inputOutput = ";";
        }
        else if (this.isInput()) {
            inputOutput = "";
        }
        sb.append(inputOutput);
    }
}
