/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.caldwellsoftware.dice;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author bencaldwell
 */
public class Genotype {
    private List<Gene> outputs;
    private final List<Gene> inputs;
    private int maxAllowedDepth = 5;
    private final Supplier<? extends Gene> geneSupplier;
    private final Random rand;
    static final Logger logger = Logger.getLogger(Genotype.class.getName());
    private double fitness = 0.0;
    private final int generationNum;
    private final int individualNum;
    private double mutationProbability = 0.1;
    private double argVariance = 0.1;
    
    public Genotype(    final List<Gene> inputs, 
                        final List<Gene> outputs, 
                        Supplier<? extends Gene> geneSupplier,
                        int maxDepth,
                        Random rand,
                        int generationNum,
                        int individualNum,
                        double mutationProbability,
                        double argVariance) {
        
        this.rand = rand;
        this.geneSupplier = geneSupplier;
        this.maxAllowedDepth = maxDepth;
        this.generationNum = generationNum;
        this.individualNum = individualNum;
        this.mutationProbability = mutationProbability;
        this.argVariance = argVariance;

        // copy outputs for use by this genotype
        List<Gene> tempOutputs = new ArrayList<>();
        outputs.stream().forEach((output) -> {
            tempOutputs.add(new Gene(output));
        });
        this.outputs = tempOutputs;
        
        // copy inputs for use by this genotype
        List<Gene> tempInputs = new ArrayList<>();
        inputs.stream().forEach((input) -> {
            tempInputs.add(new Gene(input));
        });
        this.inputs = tempInputs;
        
        // generate a random genotype with only inputs and outputs fixed
        this.generate();
    }
    
    /**
     * Genotype copy constructor.
     * @param original 
     */
    public Genotype(Genotype original, 
                    int generationNum,
                    int individualNum) {
        
        this.rand = original.rand;
        this.geneSupplier = original.geneSupplier;
        this.maxAllowedDepth = original.maxAllowedDepth;
        this.mutationProbability = original.mutationProbability;
        this.argVariance = original.argVariance;
        this.generationNum = generationNum;
        this.individualNum = individualNum;
                
        // copy outputs for use by this genotype
        List<Gene> tempOutputs = new ArrayList<>();
        original.outputs.stream().forEach((output) -> {
            Gene newGene = output.copySubTree();
            tempOutputs.add(newGene);
        });
        this.outputs = tempOutputs;
        
        // copy inputs for use by this genotype - not used now...?
        List<Gene> tempInputs = new ArrayList<>();
        original.inputs.stream().forEach((input) -> {
            tempInputs.add(new Gene(input));
        });
        this.inputs = tempInputs;
    }
    
    /**
     * Generate a genotype
     */
    private void generate() {

        /** create the tree by connecting a random
         * gene or input to the inputs of each gene
         **/
        outputs.stream().forEach((output) -> {
            output.initInputs(this::inputSupplier);
        });
    }
    
    public String printTree() {
        String tree = "";
        for (Gene output : outputs) {
                tree = tree + output.printTree();
        };
        return tree;
    }
    
    /**
     * Express the genotype as a phenotype
     */
    public void express() {
        this.outputs.stream().forEach((output) -> {
            output.express();
        });
    }
    
    /**
     * Get the number of genes used in this genotype, excluding inputs and outputs.
     * @return 
     */
    public int getGeneCount() {
        int count = 0;
        for (Gene output : outputs) {
            count += output.getGeneCount();
        }
        return count;
    }

    /**
     * supply random inputs/genes based on the current depth of the tree.
     * Inputs are more likely as the tree gets deeper.
     * @param depth
     * @return 
     */
    private Gene inputSupplier(int depth) {
        
        // shallow depth means more likely to choose a gene
        // deep depth means more likely to choose an input
        // max depth means force choosing an input
        int selection = this.rand.nextInt(this.maxAllowedDepth - depth);

        // randomly choose a gene or input
        Gene gene = null;
        if (selection == 0) {
            // selection == 0 then choose an input
            int index = this.rand.nextInt(this.inputs.size());
            gene = new Gene(this.inputs.get(index)); // always return a copied gene
        } else {
            // otherwise choose a gene
            gene = this.geneSupplier.get();
        }

        return gene;
    };

    public int getMaxDepth() {
        int depth = 0;
        for (Gene output : this.outputs) {
            int newDepth = output.getDepthInverse();
            depth = newDepth > depth ? newDepth : depth;
        }
        return depth;
    }

    public int getMaxAllowedDepth() {
        return maxAllowedDepth;
    }

    public void setMaxAllowedDepth(int maxDepth) {
        this.maxAllowedDepth = maxDepth;
    }
    

    public double getFitness() {
        return fitness;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
        logger.debug("Fitness: " + this.fitness);
    }

    /**
     * Mutate parameters by incrementing/decrementing available values.
     */
    public void mutate() {
        logger.debug(String.format("Population: %1$s, Individual: %2$s, mutate.", this.generationNum, this.individualNum));
        for (Gene output : outputs) {
            for (Gene input : output.getInputs()) {
                // mutate available parameters (gene "args") using Gaussian convolution
                input.mutateSubtreeParams(this.argVariance, this.rand);
            }
            // stochastically regenerate subtrees
            output.mutateSubtreeStructure(this.mutationProbability, this.rand, this::inputSupplier, this.maxAllowedDepth);
        }

        //TODO: delete the evolution of parameters mutation probability and argvariance
        // mutate the mutation probality parameter - this individual will change how likely it is to mutate
//        this.mutationProbability = rand.nextGaussian() * this.argVariance * this.mutationProbability + this.mutationProbability;
        // mutate the mutable argument variance - this individual will change how much it's arguments can mutate
//        this.argVariance = rand.nextGaussian() * this.argVariance * this.argVariance + this.argVariance;
    }
    
    /**
     * Swap an output subtree from one genotype to another.
     * @param a
     * @param b 
     */
    public void crossover(Genotype other) {
        // choose one (or none) output subtrees to swap between a and b
        int swap = rand.nextInt(this.outputs.size()+1) - 1;
        if (swap == -1) {
            return;
        }
        Gene aGene = this.outputs.get(swap);
        Gene bGene = other.outputs.get(swap);
        this.outputs.remove(swap);
        other.outputs.remove(swap);
        this.outputs.add(swap, bGene);
        other.outputs.add(swap, aGene);
    }

    public int getGenerationNum() {
        return generationNum;
    }

    public int getIndividualNum() {
        return individualNum;
    }

    public double getMutationProbability() {
        return mutationProbability;
    }

    public double getArgVariance() {
        return argVariance;
    }
    
    public String createGraph() {
        StringBuilder sb = new StringBuilder();
        sb.append("digraph G {");
        sb.append(System.lineSeparator());
        
        for (Gene output : this.outputs) {
            output.createGraph(sb);
        }
        
        sb.append("}");
        return sb.toString();
    }
    
    /**
     * Build an expression represented by the graph.
     * 
     * @return 
     */
    public String getExpression() {
        StringBuilder sb = new StringBuilder();
        
        for (Gene output : this.outputs) {
            output.getExpression(sb);
        }
        logger.debug("Expression: " + sb.toString());
        return sb.toString();
    }
}
