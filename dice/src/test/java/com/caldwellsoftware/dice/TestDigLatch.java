/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.caldwellsoftware.dice;

import com.caldwellsoftware.dice.Dice.Builder;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author bencaldwell
 */
public class TestDigLatch {
    
    public TestDigLatch() {
    }
    
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }
    
    @Test
    public void digLatch() throws FileNotFoundException {
        URL configURL = this.getClass().getResource("diglatch/config.xml");
        File configFile = new File(configURL.toString());
        
        Builder builder = new Dice.Builder();
        builder.setStartPopulationExpression(this::startPopulationExpression);
        builder.setEndPopulationExpression(this::endPopulationExpression);
        builder.setStartIndividualExpression(this::startIndividualExpression);
        builder.setEndIndividualExpression(this::endIndividualExpression);
        builder.setStartIndividualEvaluation(this::startIndividualEvaluation);
        //TODO: add args to test this feature
        builder.addGenes(Arrays.asList( new Gene("TON1",1, true, null, null, this::geneExpressor), 
                                        new Gene("TOFF1",1, true, null, null, this::geneExpressor), 
                                        new Gene("AND",2, true, null, null, this::geneExpressor)));
        builder.addInputs(Arrays.asList(new Gene("IN1",0, true, null, null, this::geneExpressor), 
                                        new Gene("IN2",0,true, null, null, this::geneExpressor)));
        builder.addOutputs(Arrays.asList(   new Gene("OUT1", 1, true, null, null, this::geneExpressor), 
                                            new Gene("OUT2",1, true, null, null, this::geneExpressor)));
        
        Dice dice = builder.build();
        try {
            dice.start();
        } catch (Exception ex) {
            Logger.getLogger(TestDigLatch.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void startPopulationExpression(Void a) {
        System.out.println("Call startPopulation callback.");
    }
    
    public void endPopulationExpression(Dice dice) {
        System.out.println("Call endPopulation callback.");
    }
    
    public void geneExpressor(Gene gene) {
        System.out.println("Call gene expression callback: " + gene + ". Inverse Depth is: " + gene.getDepthInverse());
    }
    
    public void startIndividualExpression(Genotype individual) {
        System.out.println("Call startIndividual callback. " + individual.getGeneCount() + " genes.");
    }
    
    public void endIndividualExpression(Genotype individual) {
        System.out.println("Call endIndividual callback.");
    }
    
    public void startIndividualEvaluation(Genotype individual) {
        System.out.println("Call getFitnessList supplier.");
        individual.setFitness(10.0);
    }
}
